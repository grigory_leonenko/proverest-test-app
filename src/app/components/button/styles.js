import styled from 'styled-components'

export const StyledButton = styled.button`
  min-width: 140px;
  height: 36px;
  border-radius: 4px;
  background-color: ${props => props.type === 'submit' ? '#2a9d8f' : '#a9a9a9'};
  color: #ffffff;
  font-size: 0.875rem;
  font-weight: 600;
  border-style: none;
  outline: none;
  cursor: pointer;
  &:hover {
    opacity: 0.8;
  }
`