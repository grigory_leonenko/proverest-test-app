import styled from 'styled-components'

export const StyledTable = styled.table`
  min-width: 100%;
  border: 0;
  white-space: nowrap;
  border-collapse: collapse;
  table-layout: fixed;
`
export const StyledTHead = styled.thead`
  padding: 0;
`

export const StyledTBody = styled.tbody`
  padding: 0;
`

export const StyledHeader = styled.th`
  padding-left: 16px;
  padding-right: 16px;
  height: 52px;
  font-size: 0.875rem;
  font-weight: 500;
  text-align: left;
  box-sizing: border-box;
`

export const StyledRow = styled.tr`
  padding: 0;
  border-top-width: ${props => props.border ? '1px' : '0'};
  border-top-style: solid;
  border-color: #d3d3d3;
`

export const StyledCell = styled.td`
  padding-left: 16px;
  padding-right: 16px;
  height: 52px;
  font-size: 0.875rem;
  font-weight: 400;
  box-sizing: border-box;
`

export const StyledIcons = styled.div`
  display: flex;
  justify-content: space-around;
  cursor: pointer;
`