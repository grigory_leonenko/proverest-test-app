import React, { useCallback } from 'react'
import {
  StyledTable,  
  StyledTHead,
  StyledTBody,
  StyledHeader,
  StyledRow,
  StyledCell,
  StyledIcons,
} from './styles'
import { EditIcon, RemoveIcon } from '../icons'

export const Table = ({ items, ...rest }) => {
  return (
    <StyledTable>
      <StyledTHead>
        <StyledRow>
          <StyledHeader>Name</StyledHeader>
          <StyledHeader>Email</StyledHeader>
          <StyledHeader>Phone</StyledHeader>
          <StyledHeader>Action</StyledHeader>
        </StyledRow>
      </StyledTHead>
      <StyledTBody>
        {items.map(item => 
          <Row 
            item={item} 
            {...rest}
          />
        )}
      </StyledTBody>
    </StyledTable>
  )
}

export const Row = ({ item, onEdit, onRemove }) => {
  const handleEdit = useCallback(() => {
    console.log('handle edit')
    onEdit(item)
  }, [item, onEdit])
  
  const handleRemove = useCallback(() => {
    onRemove(item)
  }, [item, onRemove])

  return (
    <StyledRow border>
      <StyledCell>{item.name}</StyledCell>
      <StyledCell>{item.email}</StyledCell>
      <StyledCell>{item.phone}</StyledCell>
      <StyledCell>
        <StyledIcons>
          <div onClick={handleEdit}><EditIcon /></div>
          <div onClick={handleRemove}><RemoveIcon /></div>
        </StyledIcons>
      </StyledCell>
    </StyledRow>
  )
}