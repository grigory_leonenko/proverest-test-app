import styled from 'styled-components'

export const StyledWrapper = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.4);
`

export const StyledContainer = styled.div`
  width: 500px;
  margin: 96px auto;
  border-radius: 4px;
  background-color: #ffffff;
  box-shadow: 0px 2px 1px -1px rgba(0, 0, 0, 0.2), 0px 1px 1px 0px rgba(0, 0, 0, 0.14), 0px 1px 3px 0px rgba(0,0,0,.12);
`

export const StyledTabs = styled.div`
  width: 100%;
  display: flex; 
  flex-wrap: wrap;
`

export const StyledTab = styled.div`
  width: 33.3%;
  height: 52px;
  line-height: 52px;
  font-size: 0.875rem;
  font-weight: 500;
  text-align: center;
  border-bottom-color: ${props => props.active ? '#00b4d8' : '#d3d3d3'};
  border-bottom-width: 2px;
  border-bottom-style: solid;
  cursor: pointer;
`

export const StyledContent = styled.div`
  padding: 64px 0;
`

export const StyledInput = styled.input`
  width: 200px;
  height: 30px;
  margin: 0 auto;
  display: block;
  font-size: 1rem;
  background-color: #ffffff;
  border-color: #d3d3d3;
  border-radius: 4px;
  border-style: solid;
`

export const StyledButtons = styled.div `
  padding-bottom: 32px;
  display: flex;
  justify-content: space-evenly;
`