import React, { useState, useCallback } from 'react'
import ReactDOM from 'react-dom'
import { 
  StyledWrapper, 
  StyledContainer,
  StyledTabs,
  StyledTab,
  StyledContent,
  StyledInput,
  StyledButtons,
} from './styles'
import { Button } from '../button'

const target = document.getElementById('modal')

export const Modal = ({ item, onCancel, onSave }) => {
  const [ params, setParams ] = useState(item)
  const [ tab, setTab ] = useState('name')

  const handleChangeTab = useCallback((value) => {
    setTab(value)
  }, [setTab])
  
  const handleChangeParam = useCallback((event) => {
    setParams({
      ...params,
      [tab]: event.target.value,
    })
  }, [tab, params, setParams])

  const handleSave = useCallback(() => {
    onSave(params)
  }, [params, onSave])

  const param = params[tab] || ''
  
  return ReactDOM.createPortal(
    <StyledWrapper>
      <StyledContainer>
        <StyledTabs>
          <StyledTab 
            active={tab === 'name'}
            onClick={() => handleChangeTab('name')}
          >
            Name
          </StyledTab>
          <StyledTab 
            active={tab === 'email'}
            onClick={() => handleChangeTab('email')}
          >
            Email
          </StyledTab>
          <StyledTab 
            active={tab === 'phone'}
            onClick={() => handleChangeTab('phone')}
          >
            Phone
          </StyledTab>
        </StyledTabs>
        <StyledContent>
          <StyledInput type="text" value={param} onChange={handleChangeParam} />
        </StyledContent>
        <StyledButtons>
          <Button onClick={onCancel}>Cancel</Button>
          <Button onClick={handleSave} type="submit">Save</Button>
        </StyledButtons>
      </StyledContainer>
    </StyledWrapper>,
    target
  )
}