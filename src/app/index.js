import React, { useState, useCallback, useEffect } from 'react'
import { StyledContainer, StyledButtons, GlobalStyle } from './styles'
import { Table } from './components/table'
import { Modal } from './components/modal'
import { Button } from './components/button'

export const App = () => {
  const [ items, setItems ] = useState([])
  const [ target, setTarget ] = useState(null)

  useEffect(() => {
    const stored = localStorage.getItem('items')
    if (stored) {
      setItems(JSON.parse(stored))
    }
  }, [])

  const handleEdit = useCallback((targetItem) => {
    setTarget(targetItem)
  }, [setTarget])

  const handleRemove = useCallback((targetItem) => {
    const index = items.findIndex(item => item.id === targetItem.id)
    const updated = [ ...items ]
    updated.splice(index, 1)
    setItems(updated)
    localStorage.setItem('items', JSON.stringify(updated))
  }, [items, setItems])

  const handleSave = useCallback((targetItem) => {
    let updated
    if (targetItem.id) {
      updated = items.map(item => item.id === targetItem.id ? targetItem : item)
    } else {
      updated = [ 
        ...items, 
        {
          id: Date.now().toString(16),
          ...targetItem
        }
      ]
    }
    setItems(updated)
    setTarget(null)
    localStorage.setItem('items', JSON.stringify(updated))
  }, [items, setItems, setTarget])

  const handleCancel = useCallback(() => {
    setTarget(null)
  }, [setTarget])

  const handleAdd = useCallback(() => {
    setTarget({})
  }, [setTarget])

  return (
    <React.Fragment>
      <GlobalStyle />
      <StyledContainer>
        <Table 
          items={items} 
          onEdit={handleEdit}
          onRemove={handleRemove}
        />
        <StyledButtons>
          <Button type="submit" onClick={handleAdd}>Добавить</Button>
        </StyledButtons>
      </StyledContainer>
      {target && (
        <Modal 
          item={target} 
          onCancel={handleCancel}
          onSave={handleSave}
        />
      )}
    </React.Fragment>
  )
}